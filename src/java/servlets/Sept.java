/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package servlets;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author guilherme
 */
@WebServlet(name = "Sept", urlPatterns = {"/Sept"})
public class Sept extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet Sept</title>"); 
            out.println("<link rel='stylesheet' href='style.css' type='text/css'>");
            out.println("<link rel='stylesheet' href='https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css' crossorigin='anonymous'>");
            out.println("</head>");
            out.println("<body>");
            out.println("<div class='container' style='text-align:center;'>");
            out.println("<h1 style='font-family: Verdana; color: blue; font-size: 30px;'> SEPT - SETOR DE EDUCAÇÃO PROFISSIONAL E TÉCNOLOGICA </h1>");
            out.println("<h2 style='font-family: Arial; color: black; font-size: 11px;'> R. Dr. Alcides Vieira Arcoverde, 1225 - Jardim das Américas, Curitiba - PR, 81520-260  </h2>");
            out.println("</div>");
            out.println("<div class='container'>");
            out.println("<div class='table-responsive'>");
            out.println("<table>");
            out.println("<tr>");
            out.println("<th> Curso </th>");
            out.println("<th> Página </th>");
            out.println("</tr>");
            out.println("<tr>");
            out.println("<td> Técnico em Agente Comunitário de Saúde </td>");
            out.println("<td> <a href='http://www.sept.ufpr.br/portal/agentesaude/'> http://www.sept.ufpr.br/portal/agentesaude/ </a></td>");
            out.println("</tr>");
            out.println("<tr>");
            out.println("<td> Técnico em Petróleo e Gás </td>");
            out.println("<td><a href='http://www.sept.ufpr.br/portal/petroleogas/'> http://www.sept.ufpr.br/portal/petroleogas/ </a></td>");
            out.println("</tr>");
            out.println("<tr>");
            out.println("<td> Tecnologia em Análise e Desenvolvimento de Sistemas </td>");
            out.println("<td><a href='http://www.sept.ufpr.br/portal/analisesistemas/'> http://www.sept.ufpr.br/portal/analisesistemas/ </a></td>");
            out.println("</tr>");
            out.println("<tr>");
            out.println("<td> Tecnologia em Comunicação Institucional </td>");
            out.println("<td><a href='http://www.sept.ufpr.br/portal/comunicacaoinstitucional/sobre-o-curso/'> http://www.sept.ufpr.br/portal/comunicacaoinstitucional/sobre-o-curso/ </a></td>");
            out.println("</tr>");
            out.println("<tr>");
            out.println("<td> Tecnologia em Gestão da Qualidade </td>");
            out.println("<td><a href='http://www.sept.ufpr.br/portal/gestaoqualidade/'> http://www.sept.ufpr.br/portal/gestaoqualidade/ </a></td>");
            out.println("</tr>");
            out.println("<tr>");
            out.println("<td> Tecnologia em Gestão Pública </td>");
            out.println("<td><a href='http://www.sept.ufpr.br/portal/gestaopublica/'> http://www.sept.ufpr.br/portal/gestaopublica/ </a></td>");
            out.println("</tr>");
            out.println("<tr>");
            out.println("<td> Tecnologia em Luteria </td>");
            out.println("<td><a href='http://www.sept.ufpr.br/portal/luteria/'> http://www.sept.ufpr.br/portal/luteria/ </a></td>");
            out.println("</tr>");
            out.println("<tr>");
            out.println("<td> Tecnologia em Negócios Imobiliários </td>");
            out.println("<td><a href='http://www.sept.ufpr.br/portal/negociosimobiliarios/'> http://www.sept.ufpr.br/portal/negociosimobiliarios/ </a></td>");
            out.println("</tr>");
            out.println("<tr>");
            out.println("<td> Tecnologia em Produção Cênica </td>");
            out.println("<td><a href='http://www.sept.ufpr.br/portal/producaocenica/'> http://www.sept.ufpr.br/portal/producaocenica/ </a></td>");
            out.println("</tr>");
            out.println("<tr>");
            out.println("<td> Tecnologia em Secretariado </td>");
            out.println("<td><a href='http://www.sept.ufpr.br/portal/secretariado/'> http://www.sept.ufpr.br/portal/secretariado/ </a></td>");
            out.println("</tr>");
            out.println("<tr>");
            out.println("<td> Pós-Graduação em Bioinformática </td>");
            out.println("<td><a href='http://www.bioinfo.ufpr.br'> http://www.bioinfo.ufpr.br </a></td>");
            out.println("</tr>");
            out.println("<tr>");
            out.println("<td> ESPECIALIZAÇÃO EM INTELIGÊNCIA ARTIFICIAL APLICADA </td>");
            out.println("<td><a href='http://www.iaa.ufpr.br'> http://www.iaa.ufpr.br </a></td>");
            out.println("</tr>");
            out.println("<tr>");
            out.println("<td> Especialização em Engenharia de Software </td>");
            out.println("<td><a href='http://www.engenhariadesoftware.ufpr.br:28080/engenhariadesoftware/'> http://www.engenhariadesoftware.ufpr.br:28080/engenhariadesoftware/ </a></td>");
            out.println("</tr>");
            out.println("</table>");
            out.println("</div>");
            out.println("</div>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
