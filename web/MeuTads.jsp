<%-- 
    Document   : MeuTads
    Created on : 17/08/2019, 14:09:08
    Author     : guilherme
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" crossorigin="anonymous">
        <link rel="stylesheet" href="style.css" type="text/css"/>
    </head>
    <body>
        <div class="container">
            <h1> Tecnologia em Análise e Desenvolvimento de Sistemas - TADS </h1>       
            <img src="ufpr.jpg" alt="logo" width="200" height="150">
            <div class="table-responsive">          
                <table class="table table-striped">
                  <thead>
                    <tr>
                      <th> Nome </th>
                      <th> Rede Social </th>
                    </tr>
                  </thead>
                  <tbody>
                    <tr>
                      <td> André Viktor </td>
                      <td> <a href=""/>  </td>
                    </tr>
                    <tr>
                      <td> Gleidison dos Santos </td>
                      <td> <a href=""/> </td>
                    </tr>
                    <tr>
                      <td> Guilherme Valério </td>
                      <td> <a href=""/> </td>
                    </tr>
                    <tr>
                      <td> Rafael Karam </td>
                      <td> <a href=""/> </td>
                    </tr>
                    <tr>
                      <td> Rafael Zezilia </td>
                      <td> <a href=""/> </td>
                    </tr>
                  </tbody>
                </table>
            </div>
        </div>
    </body>
</html>
